﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class CarDrive : MonoBehaviour
{
    public float speed;
    public float turnSpeed;
    public float gravityMultiplier;

    private Rigidbody rb;

    [SerializeField] private GameObject rightWheel;
    [SerializeField] private GameObject leftWheel;
    private Vector3 cartPos;

    public float speedNumber;

    public ParticleSystem sparkyL;
    public ParticleSystem sparkyR;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        sparkyL.Stop();
        sparkyR.Stop();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Accelerate();
        Turn();
        Fall();

        cartPos = gameObject.transform.rotation.eulerAngles;
    }

    private void Update()
    {
        speedNumber = rb.velocity.magnitude;

        if(Input.GetKeyUp(KeyCode.S))
        {
            sparkyL.Stop();
            sparkyR.Stop();
        }
    }

    void Accelerate()
    {
        if (Input.GetKey(KeyCode.W))
        {
            rb.AddRelativeForce(new Vector3(Vector3.forward.x, 0, Vector3.forward.z) * speed * 10);

        }
        else if (Input.GetKey(KeyCode.S))
        {
            rb.AddRelativeForce(-new Vector3(Vector3.forward.x, 0, Vector3.forward.z) * speed * 20);
            sparkyL.Play();
            sparkyR.Play();
        }

        Vector3 locVel = transform.InverseTransformDirection(rb.velocity);
        locVel = new Vector3(0, locVel.y, locVel.z);
        rb.velocity = transform.TransformDirection(locVel);
    }

    void Turn()
    {
        if (Input.GetKey(KeyCode.A))
        {
            rb.AddTorque(-transform.up * turnSpeed * 10);

            rightWheel.transform.DORotate(cartPos + new Vector3(0, -22, 0), 1);
            leftWheel.transform.DORotate(cartPos + new Vector3(0, -22, 0), 1);

        }
        else if (Input.GetKey(KeyCode.D))
        {
            rb.AddTorque(transform.up * turnSpeed * 10);

            rightWheel.transform.DORotate(cartPos + new Vector3(0, 22, 0), 1);
            leftWheel.transform.DORotate(cartPos + new Vector3(0, 22, 0), 1);
        }
        else
        {
            rightWheel.transform.DORotate(cartPos, 1);
            leftWheel.transform.DORotate(cartPos, 1);
        }
    }

    void Fall()
    {
        rb.AddForce(Vector3.down * gravityMultiplier * 10);
    }
}