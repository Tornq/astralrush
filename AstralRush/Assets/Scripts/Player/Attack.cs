﻿using UnityEngine;
using UnityEditor;
using TMPro;

public class Attack : MonoBehaviour
{

    public Animator animator;

    public float coolDown;
    public bool hasAttacked = false;
    private TextMeshProUGUI cool;

    public int collisionCount = 0;

    private void Start()
    {
        cool = GameObject.Find("Cool").GetComponent<TextMeshProUGUI>();

        animator = GameObject.Find("space_katana").GetComponent<Animator>();
    }

    private void Update()
    {

        if (collisionCount == 0)
        {
            if (Input.GetKey(KeyCode.K) && hasAttacked == false)
            {
                animator.SetBool("IsAttacking", true);
                hasAttacked = true;
                coolDown = 1;
            }
        }

        if (hasAttacked == true)
        {
            coolDown -= Time.deltaTime;
        }

        if (coolDown <= 0)
        {
            hasAttacked = false;
            coolDown = 0;
        }

        if (coolDown == 0)
        {
            animator.SetBool("IsAttacking", false);
        }

        cool.text = coolDown.ToString("0.0");
    }
}