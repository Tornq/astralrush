﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Obstacle : MonoBehaviour
{
    [SerializeField] private float timer = 0.2f;
    [SerializeField] private GameObject crash;
    [SerializeField] private GameObject car;

    public AudioSource audioSource;
    [SerializeField] private AudioClip waah;

    private void Start()
    {
       car = GameObject.Find("shopping_cart");
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            car.GetComponent<CarDrive>().enabled = false;
            audioSource.PlayOneShot(waah);
            
            crash.SetActive(true);
            Debug.Log("Death");
        }
    }
    private void Update()
    {
        if(crash.activeSelf == true)
        {
            timer -= Time.unscaledDeltaTime;

            if(timer <= 0f)
            {
                timer = 0f;
                SceneManager.LoadScene("Death");
            }
        }
    }
}
