﻿using UnityEngine;
using System.Collections;

public class Enemy : Attack
{
    [SerializeField] private GameObject indicator;

    private void OnTriggerEnter(Collider other)
    {
        collisionCount = 1;
        indicator.SetActive(true);
    }

    private void OnTriggerStay(Collider other)
    {

            if (Input.GetKey(KeyCode.K) && hasAttacked == false)
            {
                indicator.SetActive(false);
                hasAttacked = true;
                collisionCount = 0;
                gameObject.SetActive(false);
            }
    }

    private void OnTriggerExit(Collider other)
    {
        collisionCount = 0;
        indicator.SetActive(false);
    }
}
