﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour
{
    [SerializeField] private GameObject explosion;
    [SerializeField] private GameObject drone;

    private void Start()
    {
        drone = gameObject.transform.GetChild(0).gameObject;
        explosion = gameObject.transform.GetChild(1).gameObject;
    }
    // Update is called once per frame
    void Update()
    {
        if(drone.activeSelf == false)
        {
            explosion.SetActive(true);
        }
    }
}
