﻿using UnityEngine;
using System.Collections;
using System.IO;
using TMPro;

public class DisplayScore : MonoBehaviour
{
    [SerializeField] private string noScore;
    [SerializeField] private TextMeshProUGUI textMesh;

    private float loadedHighScore;
    public bool doesSaveExsist;

    private HighscoreData highscoreData;

    private string filePath;

    private void Awake()
    {
        filePath = Application.persistentDataPath + "/Highscore.json";

        doesSaveExsist = LoadHighScore();
    }

    void Start()
    {
        if(doesSaveExsist && loadedHighScore > 0)
        {
            textMesh.text = loadedHighScore.ToString("0.00");
        }
        else
        {
            textMesh.text = noScore;
        }
    }

    bool LoadHighScore()
    {
        if (File.Exists(filePath))
        {
            string loadedHighScoreData = File.ReadAllText(filePath);
            highscoreData = JsonUtility.FromJson<HighscoreData>(loadedHighScoreData);
            loadedHighScore = highscoreData.highScore;
            return true;
        }
        return false;
    }
}
