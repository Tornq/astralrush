﻿using UnityEngine;
using System.Collections;
using System.IO;
using TMPro;

public class SaveLoadHighScore : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI textMesh;

    private float loadedHighScore;
    private bool doesSaveExsist;
    private HighscoreData highscoreData;

    private string filePath;

    private float currentScore;

    public bool hasEnded = false;

    private void Awake()
    {
        filePath = Application.persistentDataPath + "/Highscore.json";
        doesSaveExsist = LoadHighScore();
    }

    private void Start()
    {
        currentScore = 0;
    }

    private void Update()
    {
        if(Time.timeScale >= 1f && hasEnded == false)
        {
            currentScore += Time.deltaTime;
        }
        if(hasEnded == true)
        {
            SaveHighScore();
        }

        textMesh.text = currentScore.ToString("0.00");
    }

    public void SaveHighScore()
    {
        if(File.Exists(filePath) == false)
        {
            string filepath = Application.persistentDataPath + "/Highscore.json";
            HighscoreData highscoreData = new HighscoreData();
            highscoreData.highScore = currentScore;
            string dataToSave = JsonUtility.ToJson(highscoreData);
            File.WriteAllText(filepath, dataToSave);
        }

        if(currentScore <= loadedHighScore && File.Exists(filePath))
        {
            string filepath = Application.persistentDataPath + "/Highscore.json";
            HighscoreData highscoreData = new HighscoreData();
            highscoreData.highScore = currentScore;
            string dataToSave = JsonUtility.ToJson(highscoreData);
            File.WriteAllText(filepath, dataToSave);
        }

        doesSaveExsist = true;
    }

    bool LoadHighScore()
    {
        if(File.Exists(filePath))
        {
            string loadedHighScoreData = File.ReadAllText(filePath);
            highscoreData = JsonUtility.FromJson<HighscoreData>(loadedHighScoreData);
            loadedHighScore = highscoreData.highScore;
            return true;
        }
        return false;
    }

    float CurrentScore
    {
        get { return currentScore; }
        set { currentScore = value; }
    }
}