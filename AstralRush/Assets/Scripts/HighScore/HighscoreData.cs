﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SerializeField]
public class HighscoreData
{
    public float highScore;
}
