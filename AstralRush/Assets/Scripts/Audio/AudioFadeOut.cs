﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class AudioFadeOut : MonoBehaviour
{
    [SerializeField] private float delay = 1f;

    private void Start()
    {
        if(AudioListener.volume == 0)
        {
            AudioListener.volume = 1;
        }
    }

    //function to be called on button click
    public void LoadNextLevel(string name)
    {
        StartCoroutine(LevelLoad(name));
    }

    //load level after one sceond delay
    IEnumerator LevelLoad(string name)
    {

        float elapsedTime = 0;
        float currentVolume = AudioListener.volume;

        while (elapsedTime < delay)
        {
            elapsedTime += Time.deltaTime;
            AudioListener.volume = Mathf.Lerp(currentVolume, 0, elapsedTime / delay);
            yield return null;
        }

        SceneManager.LoadScene(name);
    }
}