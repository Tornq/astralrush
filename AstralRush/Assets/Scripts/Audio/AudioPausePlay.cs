﻿using UnityEngine;
using System.Collections;


public class AudioPausePlay : MonoBehaviour
{
    public AudioSource audioSource;
    // two clips, perhaps songs for the game
    public AudioClip song1;
    public AudioClip song2;

    private bool paused1;
    private bool paused2;

    public GameObject callIndicator;
    [SerializeField] private GameObject tutorial;

    // both songs are in paused state
    void Start()
    {
        audioSource.clip = song1;
        audioSource.Play(0);
        paused1 = false;
        paused2 = true;
    }

    void OnTriggerEnter(Collider other)
    {
        Time.timeScale = 0f;
        callIndicator.SetActive(false);

            if (paused1 == false)
            {
                audioSource.Pause();
                paused1 = true;
            }

            if (paused2 == true)
            {
                audioSource.clip = song2;
                audioSource.Play(0);
                paused2 = false;
            }

            if(tutorial.activeSelf == false && Time.timeScale == 0f)
            {
                Time.timeScale = 1f;
            }
    }

}

