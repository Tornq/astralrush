﻿using UnityEngine;
using System.Collections;

public class AudioCheck : MonoBehaviour
{
    private void Start()
    {
        Time.timeScale = 1f;

        if (AudioListener.volume == 0)
        {
            AudioListener.volume = 1;
        }
    }
}
