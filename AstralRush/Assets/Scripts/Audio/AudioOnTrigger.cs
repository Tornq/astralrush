﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class AudioOnTrigger : MonoBehaviour
{

    public GameObject callIndicator;

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            callIndicator.SetActive(true);
            callIndicator.transform.DOShakePosition(3, 3, 10, 90).SetLoops(-1);
        }
    }

}