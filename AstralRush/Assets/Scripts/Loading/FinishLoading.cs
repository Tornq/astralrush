﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FinishLoading : MonoBehaviour
{
    public GameObject mainCam;
    public Image blackFade;
    [SerializeField] private float speedOfFade;
    [SerializeField] private float delay;
    [SerializeField] private string sceneName;

    [SerializeField] private SaveLoadHighScore saveLoadHigh;

    // Start is called before the first frame update
    void Start()
    {
        blackFade.canvasRenderer.SetAlpha(0.0f);
        delay = speedOfFade;
    }

    private void Update()
    {
        if(mainCam.GetComponent<CameraControl>().enabled == false)
        {
           Fade();


           delay -= Time.deltaTime;

           if(delay <= -6f)
           {
               SceneManager.LoadScene(sceneName);
           }

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        saveLoadHigh.hasEnded = true;

        mainCam.GetComponent<CameraControl>().enabled = false;

        saveLoadHigh.SaveHighScore();
    }

    void Fade()
    {
        blackFade.CrossFadeAlpha(1, speedOfFade, false);
    }
}
