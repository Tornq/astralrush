﻿using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    [SerializeField] private string sceneName;
    [SerializeField] private string secondSceneName;
    [SerializeField] private GameObject objectToActivate;

    public void LoadScene()
    {
        SceneManager.LoadScene(sceneName);
    }

    public void LoadSecondScene()
    {
        SceneManager.LoadScene(secondSceneName);
    }

    public void SetActive()
    {
        objectToActivate.SetActive(true);
    }

    public void Deactivate()
    {
        objectToActivate.SetActive(false);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void OpenLink()
    {
        Application.OpenURL("http://creativecommons.org/licenses/by-nc/3.0/");
    }
}