﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;

public class AsyncSceneLoading : MonoBehaviour
{
    [SerializeField] private string sceneName;

    [SerializeField] private GameObject anim;
    [SerializeField] private float endPos;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(LoadAsyncOperation());

        anim.transform.DOLocalMoveX(endPos, 6);
    }

    IEnumerator LoadAsyncOperation()
    {
        yield return new WaitForSeconds(5);

        AsyncOperation gameLevel = SceneManager.LoadSceneAsync(sceneName);

        while (gameLevel.progress < 1)
        {

            yield return new WaitForEndOfFrame();
        }
    }
}
