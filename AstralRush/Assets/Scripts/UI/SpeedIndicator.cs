﻿using UnityEngine;
using UnityEditor;

public class SpeedIndicator : MonoBehaviour
{
    public CarDrive car;

    [SerializeField] private GameObject speedOne;
    [SerializeField] private GameObject speedTwo;
    [SerializeField] private GameObject speedThree;
    [SerializeField] private GameObject speedFour;
    [SerializeField] private GameObject speedFive;

    private void Update()
    {
        if (car.speedNumber >= 200)
        {
            speedOne.SetActive(true);
        }

        if (car.speedNumber >= 500)
        {
            speedTwo.SetActive(true);
        }

        if (car.speedNumber >= 800)
        {
            speedThree.SetActive(true);
        }

        if (car.speedNumber >= 1000)
        {
            speedFour.SetActive(true);
        }

        if (car.speedNumber >= 1300)
        {
            speedFive.SetActive(true);
        }



        if (car.speedNumber <= 200)
        {
            speedOne.SetActive(false);
        }

        if (car.speedNumber <= 500)
        {
            speedTwo.SetActive(false);
        }

        if (car.speedNumber <= 800)
        {
            speedThree.SetActive(false);
        }

        if (car.speedNumber <= 1000)
        {
            speedFour.SetActive(false);
        }

        if (car.speedNumber <= 1300)
        {
            speedFive.SetActive(false);
        }
    }

}