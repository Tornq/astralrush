﻿using UnityEngine;
using System.Collections;

public class Tutorials : MonoBehaviour
{
    [SerializeField] private GameObject tutorial;

    void OnTriggerEnter(Collider other)
    {
        tutorial.SetActive(true);

        if(tutorial.activeSelf == true)
        {
            Time.timeScale = 0f;
        }
    }
}
