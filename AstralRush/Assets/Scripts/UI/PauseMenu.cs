﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


    public class PauseMenu : MonoBehaviour
    {
        public GameObject pauseMenu;

        private bool isMenuActive = false;

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (isMenuActive)
                {
                    Resume();
                }
                else
                {
                    Pause();
                }

            }

        }

        public void Pause()
        {
            pauseMenu.SetActive(true);
            isMenuActive = true;
            Time.timeScale = 0f;
        }

        public void Resume()
        {
            pauseMenu.SetActive(false);
            isMenuActive = false;
            Time.timeScale = 1f;
        }

    }




