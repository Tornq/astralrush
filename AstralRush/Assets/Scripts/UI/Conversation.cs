﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[System.Serializable]
public struct Line
{
    public Character character;

    [TextArea(2, 5)]
    public string text;
}

[CreateAssetMenu(fileName ="New Conversation", menuName = "Conversation")]
public class Conversation : ScriptableObject
{
    public Character speakerOne;
    public Character speakerTwo;
    public Line[] lines;
}
