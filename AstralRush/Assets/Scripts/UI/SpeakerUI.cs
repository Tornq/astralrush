﻿using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using TMPro;

public class SpeakerUI : MonoBehaviour
{
    public Image portait;
    public TextMeshProUGUI fullName;
    public TextMeshProUGUI dialog;

    private Character speaker;

    public Character Speaker
    {
      get => speaker;
      set {speaker = value; portait.sprite = speaker.portrait; fullName.text = speaker.fullName;}
    }
    public string Dialog {set => dialog.text = value; }

    public bool HasSpeaker()
    {
        return speaker != null;
    }

    public bool SpeakerIs(Character character)
    {
        return speaker == character;
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}