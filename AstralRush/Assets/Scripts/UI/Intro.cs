﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Intro : MonoBehaviour
{
    [SerializeField] private GameObject[] panels = new GameObject[11];
    [SerializeField] private int activatedPanelsIndex = 1;
    [SerializeField] private GameObject last;

    // Use this for initialization
    void Start()
    {
        panels[0].transform.DOLocalMoveX(0, 4);
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            GameObject panel = panels[activatedPanelsIndex];
            panel.SetActive(true);
            activatedPanelsIndex++;
        }

        if(last.activeSelf == true)
        {
            gameObject.GetComponent<Intro>().enabled = false;
        }
    }
}
