﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class RandomGenerator : MonoBehaviour
{
    int number;

    [SerializeField] private GameObject rOne;
    [SerializeField] private GameObject rTwo;
    [SerializeField] private GameObject rThree;
    [SerializeField] private GameObject rFour;
    [SerializeField] private GameObject rFive;

    // Use this for initialization
    void Awake()
    {
        number = Random.Range(1, 5);

        if(number == 1)
        {
            rOne.SetActive(true);
            rTwo.SetActive(false);
            rThree.SetActive(false);
            rFour.SetActive(false);
            rFive.SetActive(false);
        }
        if (number == 2)
        {
            rOne.SetActive(false);
            rTwo.SetActive(true);
            rThree.SetActive(false);
            rFour.SetActive(false);
            rFive.SetActive(false);
        }
        if (number == 3)
        {
            rOne.SetActive(false);
            rTwo.SetActive(false);
            rThree.SetActive(true);
            rFour.SetActive(false);
            rFive.SetActive(false);
        }
        if (number == 4)
        {
            rOne.SetActive(false);
            rTwo.SetActive(false);
            rThree.SetActive(false);
            rFour.SetActive(true);
            rFive.SetActive(false);
        }
        if (number == 5)
        {
            rOne.SetActive(false);
            rTwo.SetActive(false);
            rThree.SetActive(false);
            rFour.SetActive(false);
            rFive.SetActive(true);
        }
    }
}
