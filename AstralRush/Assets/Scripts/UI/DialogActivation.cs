﻿using UnityEngine;
using UnityEditor;

public class DialogActivation : MonoBehaviour
{
    public GameObject objectToActivate;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            objectToActivate.SetActive(true);
        }
    }
}