﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class DialogDisplay : MonoBehaviour
{
    public bool isActivatedLater;

    public Conversation conversation;

    public GameObject speakerOne;
    public GameObject speakerTwo;

    private SpeakerUI speakerUIOne;
    private SpeakerUI speakerUITwo;

    [SerializeField]
    private GameObject counter;

    private int activeLineIndex = 0;

    private void Start()
    {
        speakerUIOne = speakerOne.GetComponent<SpeakerUI>();
        speakerUITwo = speakerTwo.GetComponent<SpeakerUI>();

        speakerUIOne.Speaker = conversation.speakerOne;
        speakerUITwo.Speaker = conversation.speakerTwo;

        if(isActivatedLater == false && counter == null && conversation != null)
        {
            AdvanceConversation();
        }
    }

    private void Update()
    {
        if(isActivatedLater == false && conversation != null)
        {
            if (Input.GetKeyDown("space"))
            {
                if(counter != null)
                {
                    counter.GetComponent<ButtonPressTrack>().OnButtonPressed();

                    if (counter.GetComponent<ButtonPressTrack>().ReachedActivation() == true)
                    {
                        AdvanceConversation();
                    }
                    
                }

                if(counter == null)
                {
                    AdvanceConversation();
                }
                
            }
        }
        
    }

    void AdvanceConversation()
    {
        if(activeLineIndex < conversation.lines.Length)
        {
            DisplayLine();
            activeLineIndex += 1;
        }
        else
        {
            speakerUIOne.Hide();
            speakerUITwo.Hide();
            activeLineIndex = 0;
            if(Time.timeScale == 0f)
            {
                Time.timeScale = 1f;
            }
            if(gameObject.activeSelf == true)
            {
                conversation = null;
            }
        }
    }

    void DisplayLine()
    {
        Line line = conversation.lines[activeLineIndex];
        Character character = line.character;

        if(speakerUIOne.SpeakerIs(character))
        {
            SetDialog(speakerUIOne, speakerUITwo, line.text);
        }
        else
        {
            SetDialog(speakerUITwo, speakerUIOne, line.text);
        }
    }

    void SetDialog(SpeakerUI activeSpeakerUI, SpeakerUI inactiveSpeakerUI, string text)
    {
        activeSpeakerUI.Dialog = text;
        activeSpeakerUI.Show();
        inactiveSpeakerUI.Hide();
    }
}
