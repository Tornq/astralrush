﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;


public class ButtonPressTrack : MonoBehaviour
{

    private int buttonActivations = 0; //Amount of times the button has been pressed.

    [SerializeField] private bool onlyActivateOnce = false;
    private bool hasBeenActivated = false;

    private SceneLoader scene;

    [SerializeField] private GameObject panelOne;
    [SerializeField] private GameObject panelTwo;
    [SerializeField] private GameObject panelThree;
    [SerializeField] private GameObject panelFour;
    [SerializeField] private GameObject panelFive;
    [SerializeField] private GameObject panelSix;
    [SerializeField] private GameObject panelSeven;

    public float endPosition;

    public float tweenDurationPOne;
    
    public float tweenDurationPTwo;
    
    public float tweenDurationPThree;

    public float tweenDurationPFour;

    public float tweenDurationPFive;

    public float tweenDurationPSix;

    public float tweenDurationPSeven;

    public AudioClip ring;
    public AudioClip vibro;
    public AudioClip yellow;

    private void Start()
    {
        Debug.Log(message: "Uno");
        panelOne.transform.DOLocalMoveX(endPosition, tweenDurationPOne);
    }

    //Call this void when button is pressed
    public void OnButtonPressed()
    {
        if (!hasBeenActivated)
        {
            
            buttonActivations += 1;

            if (buttonActivations == 1)
            {
                Debug.Log(message: "Due");
                panelTwo.transform.DOLocalMoveX(endPosition, tweenDurationPTwo);
            }

            if (buttonActivations == 2)
            {
                Debug.Log(message: "Tre");
                panelThree.transform.DOLocalMoveX(endPosition, tweenDurationPThree);

                AudioSource audio = GetComponent<AudioSource>();
                audio.PlayOneShot(ring, 0.7f);
                audio.PlayOneShot(vibro, 0.7f);
            }

            if (buttonActivations == 3)
            {
                Debug.Log(message: "Quattro");
                panelFour.transform.DOLocalMoveY(endPosition, tweenDurationPFour);
                AudioSource audio = GetComponent<AudioSource>();
                audio.Stop();
                audio.PlayOneShot(yellow, 0.7f);
            }

            if (buttonActivations == 4)
            {
                Debug.Log(message: "Cinque");
                panelFive.transform.DOLocalMoveX(endPosition, tweenDurationPFive);
            }

            if (buttonActivations == 9)
            {
                Debug.Log(message: "Sei");
                panelSix.transform.DOLocalMoveX(endPosition, tweenDurationPSix);
            }

            if(buttonActivations == 10)
            {
                Debug.Log(message: "Sette");
                panelSeven.transform.DOLocalMoveX(endPosition, tweenDurationPSeven);
            }

            if(buttonActivations == 11)
            {
                hasBeenActivated = true;
                buttonActivations = 0;
            }
        }

    }

    public bool ReachedActivation()
    {
        if(buttonActivations >= 4)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
